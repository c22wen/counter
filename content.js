
// SLIDESHOW FEATURE ========================================================
var myIndex = 0;
carousel();

function carousel() {
  var x = document.getElementsByClassName("mySlides");
  for (var i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  myIndex++;
  if (myIndex > x.length) {
    myIndex = 1
  }
  x[myIndex - 1].style.display = "block";
  setTimeout(carousel, 3000); 
}

// TIMER FEATURE ============================================================

var start = new Date("April 22, 2018 23:00:00").getTime();
var x = setInterval(
  function () {
    var now = new Date().getTime();
    var distance = now - start;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    document.getElementById("timer").innerHTML = days + " days together &#x2764 ";
  }, 1000
);


